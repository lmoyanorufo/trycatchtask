# Task 1 #
It includes all points and extra points given in document PHPassignment.


# Task 2 and Task 3 #

For running both task is necesary to define a virtual host pointing to directory task2 or better task 3 for test full project.

Example for include in sites-available in Apache config.

```
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot "/var/www/TRYCATCHRepo/Task3"
    ServerName taskstrycatch.com
	
    <Directory "/var/www/TRYCATCHRepo/Task3">
        Order allow,deny
        Allow from all
        AllowOverride None
        <IfModule mod_rewrite.c>
            Options -MultiViews
            RewriteEngine On
	    RewriteBase /

	    RewriteCond %{REQUEST_FILENAME} !-f
	    RewriteCond %{REQUEST_FILENAME} !-d
	    RewriteRule ^(.*)$ index.php/$1 [L]
        </IfModule>
    </Directory>
     Header set Access-Control-Allow-Origin: *
</VirtualHost>
```

Also is necessary a mysql server, for init database you can use database.sql file.

## Some features includes in the project

* Logging.
* Dispatcher.
* Basic routing, extensible for other entities.
* Namespaces.
* REST implementation.
* Validation system for all controllers.
* Error handling.
* Basic HTTP Codes.
* Autoloading

Task 3 includes Task 2 so this is the basic usage, (if we define tasktrycatch.com as our virtual host)

[GET] For obtain an address
taskstrycatch.com/address/1

[POST] For create an address, this params must be in post vars: name, phone and street
taskstrycatch.com/address/1

[PUT] For create an address, this params must be in post vars: id, name, phone and street
taskstrycatch.com/address

[DELETE] For delete an address
taskstrycatch.com/address/1

Enjoy ;)