<?php

/**
 * Main script of application.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Main
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

ini_set('display_errors', 1);
error_reporting(E_ALL);

spl_autoload_register(function ($className) {
    include_once(str_replace("\\", DIRECTORY_SEPARATOR, $className).".php");
  });

$dispatcher = new \Tools\Dispatcher();
$dispatcher->dispatch();

// $logger = \Tools\Logs\LoggerFactory::getLogger();
// $logger->info("Trying logs");

