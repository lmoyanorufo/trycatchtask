<?php

/**
 * Implementation of views, this class render
 * in JSON format arryas of data given by controller.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Views
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Views;

class JSONRenderer
{  
    private $code;
    public function __construct($code = "HTTP/1.0 200 OK") 
    {
      $this->code = $code;
    }

    public function render($data)
    {
      header("Content-type: application/json; charset=utf-8");
      header($this->code);
      echo json_encode($data);
    }
}