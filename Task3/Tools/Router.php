<?php

/**
 * This class use the request information to decide which
 * controller and type of view to render a response. It make a router rol.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Tools
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Tools;

use Controllers;
use Views;
use Tools\Logs;

class Router
{  
  protected $request;
  protected $logger;

  public function __construct($request) 
  {
    $this->request = $request;
    $this->logger = Logs\LoggerFactory::getLogger();
  }

  public function getController()
  {
    // We obtain the name of controller class to execute.

    // All the responses are in JSON format.
    $view = new Views\JSONRenderer();

    $nameEntity = ucfirst(strtolower($this->request->getResource()));

    $controllerNamespace = "Controllers\\".$nameEntity."\\";

    $controllerClassName = $controllerNamespace;
    
    if ($this->request->isGET()){
      $controllerClassName.= "ReadController";
    }

    else if ($this->request->isPOST()){
      $controllerClassName.= "CreateController";
    }

    else if ($this->request->isPUT()){
      $controllerClassName.= "UpdateController";
    }

    else if ($this->request->isPUT()){
      $controllerClassName.= "UpdateController";
    }

    else if ($this->request->isDELETE()){
      $controllerClassName.= "DeleteController";
    }
    else{
      throw new \Exception("Not allowed method");
    }

    // Check if class exists...

    if (!class_exists($controllerClassName)){
      // Or throw an exception...
      return false;
    }

    $this->logger->info(__CLASS__." returning ".$controllerClassName);
    
    return new $controllerClassName($this->request->getParams(),$view);
  }
}