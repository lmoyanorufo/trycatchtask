<?php

/**
 * This class is use to establish connection with database, 
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Tools
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Tools;


class Connection
{ 
     
    private static $cont  = null;
     
    public function __construct() {
        die('Static class');
    }
     
    public static function connect()
    {
       // One connection through whole application
      $logger = Logs\LoggerFactory::getLogger();

      if ( null == self::$cont ){     
        try
        {
          $configApp = new ConfigApp(); // Get instance.
          $dbHost =  $configApp->getConfigVar("dbHost");
          $dbName =  $configApp->getConfigVar("dbName");
          $dbUsername = $configApp->getConfigVar("dbUserName");
          $dbUserPassword =  $configApp->getConfigVar("dbUserPassword");

          self::$cont =  new \PDO("mysql:host=".
            $dbHost.";"."dbname=".
            $dbName, 
            $dbUsername, 
            $dbUserPassword,
            array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
            );

          $logger->info("Connection made successfully");
        }
        catch(\PDOException $e){
          $logger->fatal("Error in database connection ".$e->getMessage());
          throw new \Exception("Error in database connection ".$e->getMessage());
        }
       }
      return self::$cont;
    }
     
    public static function disconnect()
    {
        self::$cont = null;
    }
}