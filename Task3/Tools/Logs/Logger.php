<?php

/**
 * Abstract class logger, logging can be in database,file or another system.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Tools\Logs
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Tools\Logs;

abstract class Logger
{ 
  const NONE = 1;
  const INFO = 1;
  const WARN = 2;
  const FATAL = 3;
  
  protected $logLevel;

  public function __construct($logLevel)
  {
    $this->logLevel = $logLevel;
  }

  public function info($message)
  {
    if ($this->logLevel >= self::INFO){
      $this->writeMessage("[INFO] ".$message);
    }
  }

  public function warn($message)
  {
    if ($this->logLevel >= self::WARN){
      $this->writeMessage("[WARN] ".$message);
    }
  }

  public function fatal($message)
  {
    if ($this->logLevel >= self::FATAL){
      $this->writeMessage("[FATAL] ".$message);
    }
  }

  public function writeMessage($message)
  {
    $this-> writeMessageImpl($this->formatMessage($message));
  }

  public abstract function writeMessageImpl($message);

  protected function formatMessage($message)
  {
    return date("Y-m-d H:i:s")." ".$message;
  }
}