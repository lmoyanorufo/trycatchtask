<?php

/**
 * Implementacion for logging in a file.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Tools\Logs
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Tools\Logs;
use Tools;

class LoggerFactory
{ 
  private static $instanceLogger;

  public static function getLogger()
  { 
    // is_writable

    if (self::$instanceLogger == null){
      
      $configApp = new Tools\ConfigApp(); // TODO Get instance.
      $loggerType =  $configApp->getConfigVar("loggerType");
      $logLevel = $configApp->getConfigVar("logLevel");
      
      if ($loggerType == "file"){ // TODO: Check if writable
        $logFile = $configApp->getConfigVar("logFile");
        self::$instanceLogger = new \Tools\Logs\LoggerFile($logLevel,$logFile);
      }
      else{
        self::$instanceLogger = new \Tools\Logs\EmptyLogger($logLevel);
      }
    }

    return self::$instanceLogger;
  }
}