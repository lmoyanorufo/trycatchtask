<?php

/**
 * Implementacion for logging in a file.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Tools\Logs
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Tools\Logs;

class LoggerFile extends Logger
{ 
  private $logFile;

  public function __construct($logLevel,$logFile)
  {
    parent::__construct($logLevel);
    $this->logFile = $logFile;
  }

  public function writeMessageImpl($message)
  {
    if (is_string($message)) {
      file_put_contents($this->logFile, $message."\n",FILE_APPEND);
    }
  }
}