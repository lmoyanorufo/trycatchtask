<?php

/**
 * This class recover all data, method, request uri, etc
 * from a request.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Tools
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Tools;

use Controllers;
use Views;

class Request
{  
  protected $methodRequest;
  protected $resource;
  protected $params;

  public function __construct() 
  {
    $this->methodRequest = $_SERVER['REQUEST_METHOD'];
    $this->params = array();
    $this->resource = false;

    $path = "";

    if (isset($_SERVER['REQUEST_URI'])){
      $path = $_SERVER['REQUEST_URI'];
    }

    $listParams = explode("/",trim($path, '/'));
    $controllerName = "";
    $paramValue = "";

    switch(sizeof($listParams)){

      case 1:
        $this->resource = $listParams[0];
        break;

      case 2:
        // For GET and DELETE
        $this->resource = $listParams[0];
        $this->params['id'] = $listParams[1];
        break;
    }

    if ($this->isPOST()){
      $this->params = $_POST;
    }

    if ($this->isPUT()){
      parse_str(file_get_contents("php://input"),$this->params);
    }

  }
  
  public function getRequestMethod()
  {
    return $this->methodRequest;
  }

  public function isGET()
  {
    return $this->methodRequest == "GET";
  }

  public function isPOST()
  {
    return $this->methodRequest == "POST";
  }

  public function isPUT()
  {
    return $this->methodRequest == "PUT";
  }

  public function isDELETE()
  {
    return $this->methodRequest == "DELETE";
  }

  public function getParams()
  {
    return $this->params;
  }

  public function getResource()
  {
    return $this->resource;
  }

  public function toString()
  {
    return "Info request [".$this->getRequestMethod()."] ".$this->resource." (Params) ".print_r($this->params,true);
  }
}