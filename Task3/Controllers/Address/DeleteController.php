<?php

/**
 * This class is a controller implementation for delete a row from
 * address entity.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Controllers\Address
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Controllers\Address;

use Entities;
use Exceptions;
use Controllers;

class DeleteController extends Controllers\Controller
{ 
  public function getDataModel()
  {
    $id = $this->data['id'];
    $addressesEntity = new Entities\Address();
    $result =  $addressesEntity->delete($id);
    return array();
  }

  public function validateDataInput()
  {
    $this->validateInputVarIntegerPositive("id");
  }
}