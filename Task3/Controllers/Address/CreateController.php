<?php

/**
 * This class is a controller implementation for create a row from
 * address entity.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Controllers\Address
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Controllers\Address;

use Entities;
use Exceptions;
use Controllers;

class CreateController extends Controllers\Controller
{ 
  public function getDataModel()
  {
    $name = $this->data['name'];
    $phone = $this->data['phone'];
    $street = $this->data['street'];

    $addressesEntity = new Entities\Address();
    $id =  $addressesEntity->create($name,$phone,$street);

    if (!$id){
      throw new \Exception("Error creating address"); 
    }
    else {
      $this->logger->info(__CLASS__." address created ".$id);
      return array("id"=>$id);
    }
  }

  public function validateDataInput()
  { 

    $this->validateInputVarStringLength("name",50);
    $this->validateInputVarStringLength("phone",50);
    $this->validateInputVarStringLength("street",50);
    return true;
  }
}