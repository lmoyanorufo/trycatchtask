<?php

/**
 * This class is a controller implementation for read a row from
 * address entity.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Controllers\Address
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Controllers\Address;

use Entities;
use Exceptions;
use Controllers;

class ReadController extends Controllers\Controller
{ 
  public function getDataModel()
  {
    $id = $this->data['id'];
    $addressesEntity = new Entities\Address();
    $data =  $addressesEntity->read($id);

    if (!$data){
      throw new Exceptions\NotFound("Address not found"); 
    }
    else {
      $this->logger->info(__CLASS__." retrieving data ".print_r($data,true));
      return $data;
    }
  }

  public function validateDataInput()
  {
    $this->validateInputVarIntegerPositive("id");
  }
}