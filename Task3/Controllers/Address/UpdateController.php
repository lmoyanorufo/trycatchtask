<?php

/**
 * This class is a controller implementation for update a row from
 * address entity.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Controllers\Address
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Controllers\Address;

use Entities;
use Exceptions;
use Controllers;

class UpdateController extends Controllers\Controller
{ 
  public function getDataModel()
  {
    $id = $this->data['id'];
    $name = $this->data['name'];
    $phone = $this->data['phone'];
    $street = $this->data['street'];

    $addressesEntity = new Entities\Address();
    $addressesEntity->update($id, $name, $phone, $street);

    return array();
  }

  public function validateDataInput()
  { 
    $this->validateInputVarIntegerPositive("id");
    $this->validateInputVarStringLength("name",50);
    $this->validateInputVarStringLength("phone",50);
    $this->validateInputVarStringLength("street",50);
    return true;
  }
}