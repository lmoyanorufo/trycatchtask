<?php

/**
 * Controller parent class, recover data from the model and
 * and render data with the given view.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Controllers
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Controllers;

use Entities;
use Tools;
use Exceptions;

abstract class Controller
{
  protected $data;
  protected $view;
  protected $logger;

  public function __construct($data,$view)
  {
    $this->data = $data;
    $this->view = $view;
    $this->logger = Tools\Logs\LoggerFactory::getLogger();
  }

  public abstract function getDataModel();
  public abstract function validateDataInput();

  public function validateInputVarStringLength($nameVar,$maxLength)
  { 
    if (!isset($this->data[$nameVar])){
      throw new Exceptions\BadRequest($nameVar." missing");
    }

    if (strlen($this->data[$nameVar]) > $maxLength){
      throw new Exceptions\BadRequest($nameVar." must be ".$maxLength." characters or less");
    }
  }

  public function validateInputVarIntegerPositive($nameVar)
  { 
    if (!isset($this->data[$nameVar])){
      throw new Exceptions\BadRequest($nameVar." missing");
    }
    
    if (!is_numeric($this->data[$nameVar]) || 
        !$this->data[$nameVar]> 0){
      throw new Exceptions\BadRequest($nameVar." must be integer positive");
    }

  }

  public function execute()
  { 
    $this->validateDataInput();
    $data = $this->getDataModel();
    $this->view->render($data);
  }
}