<?php
	
	$limit = 10;

	// For obtain n+1 numbers of Fibonnaci series
	function fibonacciPrint($n,&$acumValues) 
	{ 
		if($n < 2){
			$acumValues[$n] = $n;
			return $n;
		}	
		else {

			if (isset($acumValues[$n])){
				$ret = $acumValues[$n];
			}
			else{
				$ret = fibonacciPrint($n - 1,$acumValues) + fibonacciPrint($n - 2,$acumValues);
				$acumValues[$n] = $ret;
			}
			return $ret; 
		}
	} 

	$listValues = array();
	fibonacciPrint($limit - 1,$listValues);
	ksort($listValues);
	echo implode($listValues,",")."\n";