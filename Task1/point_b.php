<?php
	
	if (sizeof($argv) != 3){
		echo "You have to specify a base and an index. ex: ".$argv[0]." 5 2\n";
		exit(0);
	}


	$base = $argv[1];
	$index = $argv[2];

	echo "With the homemade method: ".power($base, $index)."\n";
	echo "With PHP function: ".pow($base, $index)."\n";

	function power($number, $index)
	{	
		$result = $number;

		// Basic cases
		if ($index == 1){
			return $result;
		}

		if ($index == 0){
			return 1;
		}

		if ($number == 0){
			return 0;
		}

		for ($i = 1; $i < $index;$i++)
		{
			$result = mult($result,$number);
		}

		return $result;
	}
	
	function mult($number1,$number2)
	{

		$result = 0;
		for ($i = 0; $i < $number1;$i++)
		{
			$result+= $number2;
		}

		return $result;
	}