<?php
	/*
		Iterative version of Fibonnaci series
	*/

	$n = 10; // Number of limit.

	// Basic cases.
	$fib1 = 0;
	$fib2 = 1;

	// Print serie
	for($i=0; $i<$n; $i++) 
	{ 
		if ($i == 0 || $i == 1){
			echo $i;
		}

		else {
			$nextFib = $fib1 + $fib2;
			echo $nextFib;
			$fib1 = $fib2;
			$fib2 = $nextFib;
		}

		if ($i + 1 < $n){
			echo ",";
		}
	}

	echo "\n";