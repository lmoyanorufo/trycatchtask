<?php
	
	// In this case, we have only to modify
	// one number of the point_a.

	$limit = 1000;
	echo "Total ".(sumOfMultiples(3,$limit) + sumOfMultiples(4,$limit) - sumOfMultiples(12,1000))."\n";
	
	function sumOfMultiples($number,$limit)
	{
		$acum = 0;
		
		for ($i = $number; $i < $limit;$i = $i + $number){
			$acum+=$i;
		}

		return $acum;
	}