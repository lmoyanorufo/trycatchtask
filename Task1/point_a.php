<?php
	
	// brute-force method
	$limit = 1000;
	$acum = 0;

	for($i = 0;$i < $limit;$i++){

		if ($i%3 == 0 || $i%5 == 0){
			$acum += $i;
			
		}
	}
	echo "Total ".$acum."\n";

	/*---------------------------------------*/

	// A more efficient solution ;) 
	
	// We add all the multiples of 3 and 5 and exclude
	// the repeated multiples (multiples in common)

	echo "Total ".(sumOfMultiples(3,$limit) + sumOfMultiples(5,$limit) - sumOfMultiples(15,1000))."\n";
	
	function sumOfMultiples($number,$limit)
	{
		$acum = 0;
		
		for ($i = $number; $i < $limit;$i = $i + $number){
			$acum+=$i;
		}

		return $acum;
	}