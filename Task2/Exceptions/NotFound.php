<?php

/**
 * This exception class is for all cases for data or resources not found in application
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Exceptions
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Exceptions;

class NotFound extends \Exception
{  
  
}