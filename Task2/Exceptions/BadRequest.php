<?php

/**
 * This exception class is for request with invalid data
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Exceptions
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Exceptions;

class BadRequest extends \Exception
{  
  
}