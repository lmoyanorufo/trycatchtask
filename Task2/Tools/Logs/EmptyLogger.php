<?php

/**
 * Null logger only, used as stub.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Tools\Logs
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Tools\Logs;

class EmptyLogger extends Logger
{ 
  
  public function __construct($logLevel)
  {
    parent::__construct($logLevel);
  }

  public function writeMessageImpl($message)
  {

  }
}