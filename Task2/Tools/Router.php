<?php

/**
 * This class use the request information to decide which
 * controller and type of view to render a response. It make a router rol.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Tools
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Tools;

use Controllers;
use Views;

class Router
{  
  protected $request;

  public function __construct($request) 
  {
    $this->request = $request;
  }

  public function getController()
  {
    if ($this->request->getResource() == 'address' && $this->request->isGET()){
      $view = new Views\JSONRenderer();
      return new Controllers\Address($this->request->getParams(),$view);
    }
    else{
      // Or throw an exception...
      return false;
    }
  }
}