<?php

/**
 * This class is the entry point to dispatch all request
 * of the webapp.
 * 
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Tools
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Tools;

use Controllers;
use Views;
use Tools;
use Exceptions;

class Dispatcher
{  
  protected $request;
  protected $logger;

  public function __construct() 
  {
    $this->request = new Tools\Request();
    $this->logger = Tools\Logs\LoggerFactory::getLogger();  
  }
     
  public function dispatch()
  {
    $router = new Router($this->request);
    $controller = $router->getController();

    $this->logger->info(__CLASS__." ".$this->request->toString());

    if ($controller){

      try{
          $controller->execute();
        }
        catch(Exceptions\BadRequest $e){
          $this->displayBadRequest($e->getMessage());
        }
        catch(Exceptions\NotFound $e){
          $this->displayNotFound($e->getMessage());
        }
        catch(\Exception $e){
          $this->displayError($e->getMessage());
        }
    }
    else{
      $this->displayNotFound("No match controller ".$this->request->getResource());
    }
  }

  public function displayBadRequest($message)
  {
    $view = new Views\JSONRenderer('HTTP/1.1 400 Bad Request');
    $view->render(array("error"=>"INVALID_DATA",'message'=>$message));
  }

  public function displayNotFound($message)
  {
    $view = new Views\JSONRenderer('HTTP/1.1 404 Not Found');
    $view->render(array("error"=>"NOT_FOUND",'message'=>$message));
  }

  public function displayError($message)
  { 
    $view = new Views\JSONRenderer('HTTP/1.0 500 Internal Server Error');
    $view->render(array("error"=>"ERROR",'message'=>$message));
  }
}