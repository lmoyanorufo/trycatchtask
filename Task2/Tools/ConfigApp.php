<?php

/**
 * This class read config.json and let check values for config variables.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Tools
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Tools;

class ConfigApp
{
    private $configVars;
     
    public function __construct() {
      // Must be singleton??

      $file = json_decode(file_get_contents("config.json"));
      $this->configVars = array();

      foreach($file as $key=>$value){
        $this->configVars[$key] = $value;
      }
    }
     
    public function getConfigVar($nameVar)
    {
        // TODO: Check var if exists...
        return $this->configVars[$nameVar];
    }
}