<?php

/**
 * Controller parent class, recover data from the model and
 * and render data with the given view.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Controllers
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Controllers;

use Entities;
use Tools;

abstract class Controller
{
  protected $data;
  protected $view;
  protected $logger;

  public function __construct($data,$view)
  {
    $this->data = $data;
    $this->view = $view;
    $this->logger = Tools\Logs\LoggerFactory::getLogger();
  }

  public abstract function getDataModel();
  public abstract function validateDataInput();

  public function execute()
  { 
    $this->validateDataInput();
    $data = $this->getDataModel();
    $this->view->render($data);
  }
}