<?php

/**
 * This class is a controller implementation for read a row from
 * address entity.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Controllers
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Controllers;

use Entities;
use Exceptions;

class Address extends Controller
{ 
  public function getDataModel()
  {
    $id = $this->data['id'];
    $addressesEntity = new Entities\Address();
    $data =  $addressesEntity->read($id);

    if (!$data){
      throw new Exceptions\NotFound("Address not found"); 
    }
    else {
      $this->logger->info(__CLASS__." retrieving data ".print_r($data,true));
      return $data;
    }
  }

  public function validateDataInput()
  {
    if (isset($this->data['id']) &&
        is_numeric($this->data['id']) &&
        $this->data['id']>= 0
       ){
      return true;
    }
    else {
      throw new Exceptions\BadRequest("Invalid id,must be integer positive");
    }
  }
}