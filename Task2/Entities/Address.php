<?php
	
/**
 * This class is used to access the datastore of addresses information.
 *
 * PHP version 5.3
 *
 * LICENSE: X
 *
 * @package    Entities
 * @author     Luis Moyano Rufo <luismoyanorufo@gmail.com>
 */

namespace Entities;

use Tools;

// TODO: extend from a parent class 'Entity'
class Address
{
  protected $database;

  public function __construct()
  {
  	$this->database = Tools\Connection::connect();
  }

  public function __destruct()
  {
  	Tools\Connection::disconnect();
  }

  public function read($id)
  {
      $sql = 'SELECT * FROM address WHERE id = ?';
      $q = $this->database->prepare($sql);
    	$q->execute(array($id));
    	$data = $q->fetch(\PDO::FETCH_ASSOC);
    	return $data;// Use bean??
  }
}